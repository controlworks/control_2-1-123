import matplotlib.pyplot as plt
def show_chart(simpson, trapezoid, rectangle, function):
    fig, ax = plt.subplots()
    ax.plot(simpson, label="Симпсон")
    ax.plot(trapezoid, label="Трапеция")
    ax.plot(rectangle, label="Прямоугольник")
    ax.plot(function, label="Реальное значение")
    ax.set_ylabel('Ось Y')
    ax.set_xlabel('Ось X')
    ax.set_title('Результаты расчетов')
    ax.legend()
    plt.show()