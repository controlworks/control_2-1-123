import ModChart
import ModFunction
import math


def main():
    while True:
        processor()
        if input('Для продолжения введите "y", для выхода "n"') == "n":
            break


def processor():
    minimal = 0
    maximum = math.pi / 4
    step = 1000
    ModChart.show_chart(calc_simpson(ModFunction.func, minimal, maximum, 2002),
               calc_trapezoid(ModFunction.func, minimal, maximum, step),
               calc_rectangle(ModFunction.func, minimal, maximum, step),
               functi())


def calc_trapezoid(function, mini, maxi, steps):
    """Главное интегрирование функции методом Трапеций"""
    mass = []
    dx = (maxi - mini) / steps
    res = 0
    x = mini
    for i in range(steps):
        res += dx * (function(x) + function(x + dx)) / 2
        x += dx
        mass.append(res)
    return mass


def calc_rectangle(function, mini, maxi, steps):
    """Главное интегрирование функции методом Прямоугольников"""
    dx = (maxi - mini) / steps
    res = 0
    x = mini
    mass = []
    for i in range(steps):
        res += dx * function(x)
        x += dx
        mass.append(res)
    return mass


def calc_simpson(function, mini, maxi, steps):
    """Главное интегрирование функции методом Сипсона"""
    dx = (maxi - mini) / steps
    res = 0
    x = mini
    for i in range(1, int(steps / 2 + 1)):
        res += 4 * function(x)
        x += 2 * dx

    x = mini + 2 * dx
    mass = []
    for i in range(1, int(steps / 2)):
        res += 2 * function(x)
        x += 2 * dx
        u = (dx / 3) * (function(mini) + function(mini) + res)
        mass.append(u)
    return mass


def functi():
    mass = []
    for i in range(1000):
        mass.append(ModFunction.func(0))

    return mass


main()